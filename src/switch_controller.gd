extends Node

signal puzzle_solved

@export var switch_nodes: Array
@export var animation_player: AnimationPlayer

const CORRECT_ORDER : Array = [
	0, 2, 1
]

var switches: Array
var enable_order: Array
var verifying: bool = false


func _ready():
	enable_order = Array()
	switches = Array()

	for i in range(0, switch_nodes.size()):
		var switch: Switch = get_node(switch_nodes[i]) as Switch
		switches.append(switch)
		switch.state_changed.connect(_on_switch_state_changed.bind(i))


func _on_switch_state_changed(enabled: bool, switch_number: int) -> void:
	print_debug("switch %s enabled: %s" % [switch_number, enabled])

	if !enabled:
		return

	enable_order.append(switch_number)

	if enable_order.size() >= switches.size():
		verify_correct_order()


func verify_correct_order() -> void:
	if verifying:
		return

	verifying = true

	var ok: bool = true
	var i: int = 0

	while i < switches.size() && ok:
		ok = ok && CORRECT_ORDER[i] == enable_order[i]
		i += 1

	print_debug("verification result: %s" % ok)

	if ok:
		animation_player.play("puzzle_solved_anim")
	else:
		reset()
	
	verifying = false


func reset() -> void:
	for s in switches:
		s.disable()

	enable_order = Array()
	print_debug("switch controller state has been reset")


func _process(_delta):
	pass
