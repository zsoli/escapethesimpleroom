class_name Player
extends CharacterBody3D

# nodes
@export var head: Node3D
@export var raycast: RayCast3D

## jump multiplier
@export var jump_height_multiplier: float = 15.0

## jump multiplier
@export var sprint_multiplier: float = 1.5

## gravity strength
@export var gravity_multiplier: float = 50.0 

## velocity deacceleration when jumping
@export var jump_deacceleration: float = 0.02

## velocity deacceleration when on floor
@export var floor_deacceleration: float = 0.25

## velocity acceleration when on floor
@export var floor_acceleration: float = 10.0

## mouse sensitivity multiplier
@export var mouse_acceleration_multiplier: float = 1.0

## how much can we look up and down in degrees
@export var LOOK_UP_DOWN_LIMIT: float = PI / 2.0

## smoothness of item pickup animation
@export var ITEM_POS_SMOOTHNESS: float = 0.25

const GRAVITY_DIRECTION := Vector3.DOWN
const MOUSE_SENSITIVITY_BASE: float = 0.06 * -1.0
const EPSILON = 0.001 # error threshold

var highlighted_item: Node
var target_velocity := Vector3.ZERO
var movement_accel: Vector3
var jump_accel: Vector3
var gravity: Vector3
var mouse_accel: float
var slope_angle: float
var int_is_on_floor: int

# timers
var fall_t: float = 0.0
var t: float = 0.0
var on_floor_t: float
var jump_t: float


# keyboard events handler
func proc_input() -> void:
	var event := Input

	var z1: float = event.is_action_pressed("move_backward")
	var z2: float = event.is_action_pressed("move_forward")
	var x1: float = event.is_action_pressed("move_right")
	var x2: float = event.is_action_pressed("move_left")
	var y: float = event.is_action_just_pressed("jump")
	var sprint: float = event.is_action_pressed("sprint")
	# var crouch: float = event.is_action_just_pressed("crouch")

	# movement along x and z axis
	target_velocity = (z1 - z2) * transform.basis.z + (x1 - x2) * transform.basis.x
	target_velocity = target_velocity.normalized()
	
	# apply speed modifier
	target_velocity *= movement_accel * (1 + (sprint_multiplier - 1) * sprint)

	# movement along y axis (jump)
	target_velocity += jump_accel * y * int_is_on_floor * int(jump_t > 0.5)

	# reset the jump timer if pressed jump and in air
	jump_t *= 1 - (y * int_is_on_floor * int(jump_t > 0.5))


func proc_movement(delta: float) -> void:
	var d: float = lerp(jump_deacceleration, floor_deacceleration, int_is_on_floor)
	
	target_velocity += gravity * delta

	velocity.x = lerp(velocity.x, target_velocity.x, d)
	velocity.y += target_velocity.y
	velocity.z = lerp(velocity.z, target_velocity.z, d)

	self.move_and_slide()

func check_raycast() -> void:
	var item: Switch = raycast.get_collider() as Switch

	if item == null:
		highlighted_item = null
		return
	
	item.highlight()
	highlighted_item = item
	

# mouse events handler
func handle_mouse_input(event) -> void:
	if !(event is InputEventMouseMotion) || Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	
	var rel = event.relative * mouse_accel
	head.rotate_x(deg_to_rad(rel.y))
	rotate_y(deg_to_rad(rel.x))
	head.rotation.x = clamp(head.rotation.x, -LOOK_UP_DOWN_LIMIT, LOOK_UP_DOWN_LIMIT)


func handle_interact(event) -> void:
	if !event.is_action_pressed("interact") || highlighted_item == null:
		return

	highlighted_item.enable()


func _input(event) -> void:
	handle_mouse_input(event)
	handle_interact(event)


func _process(delta) -> void:
	jump_t += delta
	fall_t += delta 


func _physics_process(delta) -> void:
	on_floor_t += delta

	if (int_is_on_floor && on_floor_t > 0.25) || (!int_is_on_floor && on_floor_t > EPSILON):
		on_floor_t = 0
		int_is_on_floor = int(is_on_floor())

	check_raycast()
	proc_input()
	proc_movement(delta)
	

func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	movement_accel = Vector3.ONE * floor_acceleration
	jump_accel = Vector3.UP * jump_height_multiplier
	gravity = GRAVITY_DIRECTION * gravity_multiplier
	mouse_accel = MOUSE_SENSITIVITY_BASE * mouse_acceleration_multiplier
