class_name PlayerCamera
extends Camera3D

@export var player: Node3D
@export var head_blob_freq: float = 2.5
@export var head_rot_freq: float = 1.8

#var camera_shake := CameraShake.new()

var camera_y_pos: float
var target_camera_fov: float

var camera_blob_t: float = 0.0
var t: float = 0.0

var movement_pos := Vector3.ZERO
var movement_rot := Vector3.ZERO


func _ready() -> void:
	target_camera_fov = self.fov
	#camera_shake = CameraShake.new()


func zoom_event() -> void:
	var event := Input
	var z1: float = event.is_action_just_released("zoom_in")
	var z2: float = event.is_action_just_released("zoom_out")

	target_camera_fov += (z1 - z2) * 10
	target_camera_fov = clamp(target_camera_fov, 10, 120)
	self.fov = lerp(self.fov, target_camera_fov, 0.2)

 
func get_movement_pos(delta: float) -> Vector3:
	var v_len: float = Vector2(player.target_velocity.x, player.target_velocity.z).abs().length()
	var mov_ampl: float = 0.01 * v_len
	var mov_freq: float = min(head_blob_freq * v_len, head_blob_freq * 4.5)
	var moving: int = v_len > 0 + 0.001

	camera_blob_t += delta * moving
	t += delta
	t *= moving

	camera_y_pos = cos(camera_blob_t * mov_freq) * mov_ampl * player.int_is_on_floor

	# if camera_y_pos < -0.05 && audio_t > 0.2:
	# 	audio_player.play_footstep()
	# 	audio_t = 0

	if player.int_is_on_floor:
		if player.fall_t > 0.2:
			pass
			#camera_shake.add_trauma(player.fall_t * 10)
		player.fall_t = 0

	var pos_w: float = clamp(t * delta * 100, 0.1, 0.2)

	movement_pos.y = lerp(movement_pos.y, camera_y_pos, pos_w)
	return movement_pos


func get_movement_rot(delta: float) -> Vector3:
	var v_len: float = Vector2(player.target_velocity.x, player.target_velocity.z).abs().length()
	
	var rot_ampl: float = 0.04 * v_len / 40.0
	var rot_freq: float = head_rot_freq * v_len
	
	var moving: int = v_len > 0 + 0.001
	var camera_z_rot = sin(camera_blob_t * rot_freq) * rot_ampl * player.int_is_on_floor

	var rot_w: float = clamp(t * delta * 100, 0.01, 0.2)

	movement_rot.z = lerp(movement_rot.z, camera_z_rot, rot_w)
	return movement_rot


func _process(delta: float) -> void:
	zoom_event()

	# apply modifications
	#var shake: Vector3 = camera_shake.process(delta)
	var movement: Vector3 = get_movement_pos(delta)
	var rotation: Vector3 = get_movement_rot(delta)

	self.position = Vector3.ZERO
	self.position += movement

	self.rotation = Vector3.ZERO
	#self.rotation += shake
	self.rotation += rotation
