extends StaticBody3D
class_name Switch

@export var mesh_instance: MeshInstance3D
@export var animation_player: AnimationPlayer

const ENABLE_ANIM_NAME: String = "LeverEnable"

var highlight_shader: ShaderMaterial
var highlighted: bool = false
var enabled: bool = false

signal state_changed


func _ready():
	animation_player.animation_finished.connect(func(anim_name: StringName):
		if anim_name != ENABLE_ANIM_NAME:
			return
			
		enabled = !enabled
		state_changed.emit(enabled)
	)

	highlight_shader = mesh_instance.get_active_material(0).get_next_pass()


func enable() -> void:
	if enabled:
		return
	
	print_debug("enabled switch: %s" % self)
	animation_player.play(ENABLE_ANIM_NAME, -1, 2.0, false)
	#state_changed.emit(enabled)
	

func disable() -> void:
	if !enabled:
		return

	#enabled = false
	animation_player.play(ENABLE_ANIM_NAME, -1, -2.0, true)
	#state_changed.emit(enabled)


func highlight() -> void:
	highlighted = true


func _process(_delta):
	highlight_shader.set_shader_parameter("highlighted", highlighted)
	highlighted = false
