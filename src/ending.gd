extends Area3D

@export var animation_player: AnimationPlayer

func _ready():
	self.body_entered.connect(func(body: Node3D):
		print_debug("something entered")
		if body is Player:
			animation_player.play("ending_animation")
	)
