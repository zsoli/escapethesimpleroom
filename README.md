## Escape The Simple Room

A simple game about escaping a minimalistic room by solving a singular puzzle involving three levers. Made using Godot 4 beta.

![screenshot](screenshot.png)